<?php if(get_row_layout() == 'wysiwyg_text_block') { ?>
	<?php the_sub_field('content'); ?>
<?php } ?>

<?php if(get_row_layout() == 'image_block') { ?>
	<?php 
		$image = get_sub_field('image');
		$size = get_sub_field('size');
		echo wp_get_attachment_image($image, $size);
	?>
<?php } ?>

<?php if(get_row_layout() == 'map_block') { ?>
	<?php if(get_sub_field('locations')) { ?>

		<div class="acf-map">
			<?php while (the_repeater_field('locations') ) { ?>
				<?php
					$location = get_sub_field('map');
				?>
				<div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>">
					<h4><?php the_sub_field('title'); ?></h4>
				</div>
			<?php } ?>
		</div>

		<?php if(get_sub_field('show_addresses') == 1) { ?>
			<section class="page-section mid">
				<section class="container ultra no-bottom">
					<section class="cols-3">
						<?php while (the_repeater_field('locations') ) { ?>
							<?php
								$location = get_sub_field('map');
							?>
							<aside>
								<h3><?php the_sub_field('title'); ?></h3>
								<p class="address"><?php echo $location['address']; ?></p>
							</aside>
						<?php } ?>
					</section>
				</section>
		<?php } ?>

	<?php } ?>
<?php } ?>
