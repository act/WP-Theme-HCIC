var $ = jQuery.noConflict();
$(document).ready(function() {

	jQuery('.container').fitVids();

    $('.mobile-nav-trigger').click(function(e) {
        e.preventDefault();
        $(this).toggleClass('active');
        $('.mobile-nav').slideToggle();
    });

	var footerHeight = $('footer').outerHeight();
    $('.footer-fix').css('padding-bottom', footerHeight);

    $('.research-filters li a').click(function(e) {
        e.preventDefault();
        var filter = $(this).data('filter');
    });

    $('header ul:not(.sub-menu) > li.menu-item-has-children').hoverIntent(function() {
        $(this).find('> ul').slideToggle();
    });

    $('.research-grid').imagesLoaded( function() {
        if(document.location.search.length) {
            var categoryQuery = getURLParameters('research-category');
            $('.research-grid').mixItUp({
                load: {
                    filter: '.' + categoryQuery
                },
                animation: {
                    duration: 1000
                }
            });
        } else {
            $('.research-grid').mixItUp({
                animation: {
                    duration: 1000
                },
                load: {
                    filter: '.all'
                }
            });
        }
    });

    $('.search-trigger').click(function(e) {
        e.preventDefault(); 
        $('.search-block').slideToggle();
    });

});

enquire
    .register("screen and (min-width:31em)", function() { 
        $(document).ready(function() {
            
        });
    }, true)
    .register("screen and (max-width:31em)", function() { 
        $(document).ready(function() {
            
        });
    });

var ieVersion = null;
if (document.body.style['msFlexOrder'] != undefined) {
    ieVersion = "ie10";
    $('html').addClass('ie-10');
}
if (document.body.style['msTextCombineHorizontal'] != undefined) {
    ieVersion = "ie11";
    $('html').removeClass('ie-10').addClass('ie-11');
}
function getURLParameters(name) {
    return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||null
}
