<?php get_header(); ?>

	<style type="text/css">
		#menu-item-635 a {color: #434240; border-bottom: 4px solid #fff;}
		#menu-item-635:hover a {color: #265a8c; border-bottom: 4px solid #265a8c;}
	</style>

	<?php $terms = get_the_terms( $post->ID , 'research-category' );  ?>

	<section class="container ultra">
		<div class="flex-article">
			<aside class="page-main">
				<article>
					<h1 class="page-title"><?php the_title(); ?></h1>
					<p class="post-meta">
						<span><i class="fa fa-calendar"></i> <?php the_time('jS F Y'); ?></span> 
						<?php if($terms) { ?>
							<span><i class="fa fa-tags"></i> 
								<?php foreach($terms as $term) { ?>
									<a href="<?php bloginfo('url'); ?>/case-studies?category=<?php echo $term->slug; ?>"><?php echo $term->name; ?></a><em>,</em> 
								<?php } ?>
							</span>
						<?php } ?>
						<span><i class="fa fa-pencil"></i> <?php the_author(); ?></span>
					</p>
					<?php get_template_part('includes/content', 'builder'); ?>
				</article>
			</aside>
			<aside class="page-sidebar">
				<?php get_sidebar(); ?>
			</aside>
		</div>
	</section>

<?php get_footer(); ?>
